import express from 'express';
import { json } from 'body-parser';
import io from 'socket.io';
import cors from 'cors';
import { Game } from './models/game';
import { Snake } from './models/snake';
import { PowerUp } from './models/powerUp';
import { World } from './models/world';

const app = express();
const server = app.listen(3000, () => {
  console.log(`Listening on port 3000`);
});

let games = new Map<number, Game>();
games.set(1, new Game('defaultGame', new World(16, 16, new Map(), [new PowerUp(1, 1, 'grow')])));

const socketio = io(server, { origins: '*:*' });

socketio.on('connection', socket => {
  socket.on('join', id => {
    const game = games.get(id);

    if (game) {
      const self = new Snake(socket, 5, 5, 1, 0.005, []);
      const { playerList, powerUps } = game.world;

      playerList.set(self.id, self);

      socket.emit('init', {
        self: self.getSerialized(),
        world: game.world.getSerialized(self),
      });

      game.world.getPlayerList(self).forEach(player => {
        player.connection.emit('join', self.getSerialized());
      });

      socket.on('c03', updateData => {
        const { x, y, direction, tailParts } = updateData;
        self.x = x;
        self.y = y;
        self.direction = direction;
        self.tailParts = tailParts;

        let consume = false;
        let consumedPowerUp;
        let dead = false;

        consumed: for (const powerUp of powerUps) {
          if (x === powerUp.x && y === powerUp.y) {
            consume = true;
            consumedPowerUp = powerUp;
            break consumed;
          }

          for (const tailPart of tailParts) {
            if (tailPart.x === powerUp.x && tailPart.y === powerUp.y) {
              consume = true;
              consumedPowerUp = powerUp;
              break consumed;
            }
          }
        }

        if (consume && consumedPowerUp) {
          self.connection.emit('consume', consumedPowerUp);

          powerUps.splice(powerUps.indexOf(consumedPowerUp), 1);
          powerUps.push(new PowerUp(getRandom(1, game.world.width - 1), getRandom(1, game.world.height - 1), 'grow'));

          game.world.getPlayerList().forEach(player => {
            player.connection.emit('updatePowerUps', powerUps);
          });
        } else {
          died: for (const snake of game.world.getPlayerList()) {
            if (snake.alive && snake.id !== self.id && x === snake.x && y === snake.y) {
              dead = true;
              break died;
            }

            for (const tailPart of snake.tailParts) {
              if (snake.alive && x === tailPart.x && y === tailPart.y) {
                dead = true;
                break died;
              }
            }
          }

          if (dead) {
            self.alive = false;
            game.world.getPlayerList().forEach(player => {
              player.connection.emit('death', self.id);
            });
          }
        }

        game.world.getPlayerList(self).forEach(player => {
          player.connection.emit('c03', self.getSerialized());
        });
      });

      socket.on('disconnect', () => {
        console.log('a');
        game.world.playerList.delete(self.id);

        game.world.getPlayerList(self).forEach(player => {
          player.connection.emit('leave', self.id);
        });
      });
    } else {
      socket.disconnect();
    }
  });
});

app.use(cors());
app.use(json({ limit: '5mb' }));

app.get('/', (req, res) => {
  let g: any[] = [];

  games.forEach((value, key) => {
    g.push({ id: key, name: value.name, player: value.world.playerList.size });
  });

  res.json(g);
});

app.post('/', (req, res) => {
  const { name, mode } = req.body;
});

app.use((req, res) => {
  res.status(404).json({
    message: `Requested route (${req.url}) not found.`,
  });
});

export default app;

function getRandom(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
