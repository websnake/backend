import { PowerUp } from './powerUp';
import { Snake } from './snake';

export class World {
  constructor(public width: number, public height: number, public playerList: Map<string, Snake>, public powerUps: PowerUp[]) {}

  getSerialized(self?: Snake) {
    return { width: this.width, height: this.height, playerList: this.getPlayerListSerialized(self), powerUps: this.powerUps };
  }

  getPlayerList(self?: Snake) {
    const playerList: Snake[] = [];

    this.playerList.forEach(snake => {
      if (!self || self.id !== snake.id) {
        playerList.push(snake);
      }
    });

    return playerList;
  }

  getPlayerListSerialized(self?: Snake) {
    const playerList: any[] = [];

    this.playerList.forEach(snake => {
      if (!self || self.id !== snake.id) {
        playerList.push(snake.getSerialized());
      }
    });

    return playerList;
  }
}
