import { World } from './world';

export class Game {
  constructor(public name: string, public world: World) {}
}
