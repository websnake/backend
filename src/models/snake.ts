import io from 'socket.io';

export class Snake {
  public id: string;
  public alive = true;

  constructor(
    public connection: io.Socket,
    public x: number,
    public y: number,
    public direction: 0 | 1 | 2 | 3,
    public speed: number,
    public tailParts: { x: number; y: number; direction: 0 | 1 | 2 | 3 }[]
  ) {
    this.id = connection.id;
  }

  getSerialized() {
    return {
      id: this.id,
      x: this.x,
      y: this.y,
      direction: this.direction,
      speed: this.speed,
      tailParts: this.tailParts,
      alive: this.alive,
    };
  }
}
