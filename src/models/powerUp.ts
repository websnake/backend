export class PowerUp {
  constructor(public x: number, public y: number, public type: 'grow' | 'speed') {}
}
